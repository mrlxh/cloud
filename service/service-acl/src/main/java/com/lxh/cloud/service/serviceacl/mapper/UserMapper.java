package com.lxh.cloud.service.serviceacl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.cloud.service.serviceacl.entity.User;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserMapper extends BaseMapper<User> {

}
