package com.lxh.cloud.service.serviceacl.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.serviceacl.entity.RolePermission;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface RolePermissionService extends IService<RolePermission> {

}
