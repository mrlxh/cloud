package com.lxh.cloud.service.serviceacl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.cloud.service.serviceacl.entity.RolePermission;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
