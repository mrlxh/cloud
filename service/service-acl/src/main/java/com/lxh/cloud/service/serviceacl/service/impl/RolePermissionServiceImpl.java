package com.lxh.cloud.service.serviceacl.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.cloud.service.serviceacl.entity.RolePermission;
import com.lxh.cloud.service.serviceacl.mapper.RolePermissionMapper;
import com.lxh.cloud.service.serviceacl.service.RolePermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
