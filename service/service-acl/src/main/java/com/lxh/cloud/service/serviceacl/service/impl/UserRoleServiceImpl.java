package com.lxh.cloud.service.serviceacl.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.cloud.service.serviceacl.entity.UserRole;
import com.lxh.cloud.service.serviceacl.mapper.UserRoleMapper;
import com.lxh.cloud.service.serviceacl.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
