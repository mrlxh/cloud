package com.lxh.cloud.service.serviceacl.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.serviceacl.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserRoleService extends IService<UserRole> {

}
