package com.lxh.edu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Lee
 * @date 2020/4/5 -16:28
 */
@MapperScan("com.lxh.edu.mapper")
@ComponentScan("com.lxh")
@EnableTransactionManagement
@SpringBootApplication
@EnableFeignClients
public class ApplicationServiceClassMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationServiceClassMain.class , args) ;
    }
}
