package com.lxh.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.common.servicebase.entity.QueryObject;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.Article;
import com.lxh.edu.entity.Course;
import com.lxh.edu.entity.vo.ArticleCmsQuery;
import com.lxh.edu.entity.vo.CourseCmsQuery;
import com.lxh.edu.entity.vo.CourseQuery;
import com.lxh.edu.service.ArticleService;
import com.lxh.edu.service.CourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author Lee
 * @date 2020/4/13 -17:09
 */
@RestController
@RequestMapping("/eduservice/index")
public class IndexController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private ArticleService articleService;


    //经常忘记配置分页插件
    @ApiOperation("分页课程基础信息")
    @Cacheable(value = "indexCourse", key = "#queryObject.current",condition = "#queryObject.current==1")
    @PostMapping("/indexCourse")
    public CommonResult indexCourse(@RequestBody QueryObject queryObject) {

        Page<Course> page = new Page<>(queryObject.getCurrent(), queryObject.getLimit());
        courseService.page(page,new QueryWrapper<>());
        long total = page.getTotal();
        List<Course> records = page.getRecords();
        return CommonResult.ok().data("total",total ).data("rows",records);
    }


    @ApiOperation("课程页面课程查询信息")
    @PostMapping("/selectCourseQuery")
    public CommonResult selectCourseQuery(@RequestBody CourseCmsQuery courseCmsQuery) {

        Page<Course> page = new Page<>(courseCmsQuery.getCurrent(), courseCmsQuery.getLimit());
        courseService.pageQuery(page,courseCmsQuery);
        long total = page.getTotal();
        List<Course> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }


    @ApiOperation("查询页面查询")
    @PostMapping("/searchCourseQuery")
    public CommonResult searchCourseQuery(@RequestBody CourseCmsQuery courseCmsQuery) {

        Page<Course> page = new Page<>(courseCmsQuery.getCurrent(), courseCmsQuery.getLimit());
        QueryWrapper<Course> courseQueryWrapper = new QueryWrapper<>();
        courseQueryWrapper.like("title",courseCmsQuery.getTitle());
        courseService.page(page,courseQueryWrapper);
        long total = page.getTotal();
        List<Course> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("查询页面查询")
    @PostMapping("/searchArticleQuery")
    public CommonResult searchArticleQuery(@RequestBody ArticleCmsQuery articleCmsQuery) {

        Page<Article> page = new Page<>(articleCmsQuery.getCurrent(), articleCmsQuery.getLimit());
        QueryWrapper<Article>  articleQueryWrapper= new QueryWrapper<>();
        articleQueryWrapper.like("title",articleCmsQuery.getTitle());
        articleService.page(page,articleQueryWrapper);
        long total = page.getTotal();
        List<Article> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

}
