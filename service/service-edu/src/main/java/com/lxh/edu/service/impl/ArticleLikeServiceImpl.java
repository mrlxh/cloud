package com.lxh.edu.service.impl;

import com.lxh.edu.entity.ArticleLike;
import com.lxh.edu.mapper.ArticleLikeMapper;
import com.lxh.edu.service.ArticleLikeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 点赞 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@Service
public class ArticleLikeServiceImpl extends ServiceImpl<ArticleLikeMapper, ArticleLike> implements ArticleLikeService {

}
