package com.lxh.edu.entity.vo;

import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/25 -10:39
 */
@Data
public class UserArticleLikeVo {

    private String userId;

    private String articleId;
}
