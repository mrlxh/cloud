package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.SysOrder;
import com.lxh.edu.service.SysOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程收藏 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-05-16
 */
@RestController
@RequestMapping("/eduservice/order")
public class SysOrderController {
    @Autowired
    private SysOrderService sysOrderService;

    @PostMapping("/addOrder")
    public CommonResult addOrder(@RequestBody SysOrder order){

        boolean flag= sysOrderService.save(order);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @PostMapping("/selectOrder")
    public CommonResult selectOrder(@RequestBody SysOrder order){
        QueryWrapper<SysOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",order.getCourseId()).and(queryWrapper -> queryWrapper.eq("member_id",order.getMemberId()));
        int count = sysOrderService.count(wrapper);
        return CommonResult.ok().data("count",count);
    }
}

