package com.lxh.edu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程简介 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@RestController
@RequestMapping("/edu/course-description")
public class CourseDescriptionController {

}

