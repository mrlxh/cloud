package com.lxh.edu.mapper;

import com.lxh.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface CourseDescriptionMapper extends BaseMapper<CourseDescription> {

}
