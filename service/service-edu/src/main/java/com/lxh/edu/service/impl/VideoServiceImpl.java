package com.lxh.edu.service.impl;

import com.lxh.edu.entity.Video;
import com.lxh.edu.entity.vo.VideoInfoVo;
import com.lxh.edu.mapper.VideoMapper;
import com.lxh.edu.service.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

    @Override
    public boolean updateVideoInfo(VideoInfoVo video) {
        int flag=baseMapper.updateVideoInfo(video);
        return flag!=0;
    }
}
