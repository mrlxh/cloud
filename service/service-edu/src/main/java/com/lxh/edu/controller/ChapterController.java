package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.Chapter;
import com.lxh.edu.entity.Video;
import com.lxh.edu.entity.tree.ChapterTreeGrid;
import com.lxh.edu.entity.vo.CourseInfoVo;
import com.lxh.edu.service.ChapterService;
import com.lxh.edu.service.VideoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@RestController
@RequestMapping("/eduservice/chapter")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private VideoService videoService;

    /*
    * 添加章节页面，，获取章节和章节下面的小节
    * */
    @ApiOperation(value = "获取课程章节")
    @PostMapping("/getChapter/{id}")
    public CommonResult getChapter(
            @ApiParam(value = "课程id",required = true)
            @PathVariable String id) {
        List<ChapterTreeGrid> list=chapterService.getChapter(id);
        return CommonResult.ok().data("data",list);
    }

    @ApiOperation(value = "添加课程章节")
    @PostMapping("/addChapter")
    public CommonResult addChapter(@RequestBody Chapter chapter) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = chapterService.save(chapter);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @ApiOperation(value = "修改课程章节")
    @PutMapping("/updateChapter")
    public CommonResult updateChapter(@RequestBody Chapter chapter) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = chapterService.updateById(chapter);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @ApiOperation(value = "删除课程章节")
    @DeleteMapping("/deleteChapter/{id}")
    public CommonResult deleteChapter(@PathVariable String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = chapterService.removeById(id);
        //把小节也删除
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        videoQueryWrapper.eq("chapter_id",id);
        videoService.remove(videoQueryWrapper);
        return flag?CommonResult.ok():CommonResult.error();
    }

}

