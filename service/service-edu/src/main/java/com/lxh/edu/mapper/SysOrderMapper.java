package com.lxh.edu.mapper;

import com.lxh.edu.entity.SysOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程收藏 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-05-16
 */
public interface SysOrderMapper extends BaseMapper<SysOrder> {

}
