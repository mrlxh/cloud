package com.lxh.edu.controller;


import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.Video;
import com.lxh.edu.entity.vo.VideoInfoVo;
import com.lxh.edu.service.VideoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@RestController
@RequestMapping("/eduservice/video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @ApiOperation(value = "添加课程章节的小节")
    @PostMapping("/addVideo")
    public CommonResult addVideo(@RequestBody Video video) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = videoService.save(video);
        return flag?CommonResult.ok():CommonResult.error();
    }


    //参数的实体类记得加@data注解。。不然获取不到值
    @ApiOperation(value = "修改课程章节的小节")
    @PostMapping("/updateVideoInfo")
    public CommonResult updateVideoInfo(@RequestBody VideoInfoVo videoInfoVo) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = videoService.updateVideoInfo(videoInfoVo);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @ApiOperation(value = "更具Id的小节")
    @PostMapping("/getById/{id}")
    public CommonResult addVideo(@PathVariable("id") String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        Video video = videoService.getById(id);
        return CommonResult.ok().data("video",video);
    }


    //先删除小节信息，，以后再完善把视频也删除//以后在把视频id传进来
    @ApiOperation(value = "删除小节")
    @DeleteMapping("/deleteById/{id}")
    public CommonResult deleteById(@PathVariable("id") String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag = videoService.removeById(id);
        return flag?CommonResult.ok():CommonResult.error();
    }
}

