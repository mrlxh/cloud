package com.lxh.edu.service.impl;

import com.lxh.edu.entity.SysOrder;
import com.lxh.edu.mapper.SysOrderMapper;
import com.lxh.edu.service.SysOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程收藏 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-05-16
 */
@Service
public class SysOrderServiceImpl extends ServiceImpl<SysOrderMapper, SysOrder> implements SysOrderService {

}
