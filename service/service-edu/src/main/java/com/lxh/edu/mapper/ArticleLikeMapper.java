package com.lxh.edu.mapper;

import com.lxh.edu.entity.ArticleLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点赞 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface ArticleLikeMapper extends BaseMapper<ArticleLike> {

}
