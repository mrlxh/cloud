package com.lxh.edu.service;

import com.lxh.edu.entity.ArticleLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点赞 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface ArticleLikeService extends IService<ArticleLike> {

}
