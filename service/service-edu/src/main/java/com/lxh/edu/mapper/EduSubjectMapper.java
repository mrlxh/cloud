package com.lxh.edu.mapper;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.edu.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-09
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

    List<BaseTreeGrid> subjectTree();
}
