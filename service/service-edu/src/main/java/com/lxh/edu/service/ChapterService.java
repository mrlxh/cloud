package com.lxh.edu.service;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.edu.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.edu.entity.tree.ChapterTreeGrid;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface ChapterService extends IService<Chapter> {

    List<ChapterTreeGrid> getChapter(String id);
}
