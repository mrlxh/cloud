package com.lxh.edu.mapper;

import com.lxh.edu.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 反馈 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

}
