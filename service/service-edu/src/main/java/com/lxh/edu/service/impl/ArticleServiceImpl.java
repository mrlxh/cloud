package com.lxh.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.edu.entity.Article;
import com.lxh.edu.entity.vo.ArticleCmsQuery;
import com.lxh.edu.entity.vo.ArticleQueryObject;
import com.lxh.edu.mapper.ArticleMapper;
import com.lxh.edu.service.ArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 文章 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-18
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Override
    public void pageQuery(Page<Article> page, ArticleCmsQuery articleCmsQuery) {

        QueryWrapper<Article> wrapper = new QueryWrapper<>();
        String subjectId = articleCmsQuery.getSubjectId();

        String subjectParentId = articleCmsQuery.getSubjectParentId();

        if (!"1".equals(articleCmsQuery.getSubjectParentId())) {
            wrapper.eq("subject_parent_id", subjectParentId);
        }

        if (!"1".equals(articleCmsQuery.getSubjectId())){
            wrapper.eq("subject_id", subjectId);
        }

        baseMapper.selectPage(page,wrapper);
    }

    @Override
    public void pageQuery(Page<Article> page, ArticleQueryObject articleQueryObject) {

        QueryWrapper<Article> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(articleQueryObject.getTitle())) {
            wrapper.eq("title", articleQueryObject.getTitle());
        }
        if (!StringUtils.isEmpty(articleQueryObject.getBegin())) {
            wrapper.ge("gmt_create", articleQueryObject.getBegin());
        }
        if (!StringUtils.isEmpty(articleQueryObject.getEnd())) {
            wrapper.le("gmt_create", articleQueryObject.getEnd());
        }
        baseMapper.selectPage(page, wrapper);
    }
}
