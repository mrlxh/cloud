package com.lxh.edu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.edu.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.edu.entity.vo.ArticleCmsQuery;
import com.lxh.edu.entity.vo.ArticleQueryObject;

/**
 * <p>
 * 文章 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-18
 */
public interface ArticleService extends IService<Article> {

    void pageQuery(Page<Article> page, ArticleCmsQuery articleCmsQuery);

    void pageQuery(Page<Article> page, ArticleQueryObject articleQueryObject);
}
