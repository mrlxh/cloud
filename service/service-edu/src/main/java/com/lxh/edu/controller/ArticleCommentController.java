package com.lxh.edu.controller;


import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.ArticleComment;
import com.lxh.edu.service.ArticleCommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@RestController
@RequestMapping("/eduservice/articleComment")
public class ArticleCommentController {

    @Autowired
    private ArticleCommentService articleCommentService;

    @ApiOperation(value = "根据文章id获取评论")
    @PostMapping("/getArticleComment/{id}")
    public CommonResult getArticleComment(@PathVariable("id") String id) {
        List<Map> list=articleCommentService.getComment(id);
        return CommonResult.ok().data("list",list);
    }


    @ApiOperation(value = "添加评论")
    @PostMapping("/addArticleComment")
    public CommonResult addArticleComment(@RequestBody ArticleComment articleComment) {
        boolean flag = articleCommentService.save(articleComment);
        return flag?CommonResult.ok():CommonResult.error();
    }
}

