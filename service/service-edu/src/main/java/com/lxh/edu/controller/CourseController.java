package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.ArticleLike;
import com.lxh.edu.entity.Course;
import com.lxh.edu.entity.UserArticle;
import com.lxh.edu.entity.UserCourse;
import com.lxh.edu.entity.vo.CourseInfoVo;
import com.lxh.edu.entity.vo.CourseQuery;
import com.lxh.edu.entity.vo.UserArticleLikeVo;
import com.lxh.edu.service.CourseService;
import com.lxh.edu.service.UserCourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@RestController
@RequestMapping("/eduservice/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserCourseService userCourseService;

    @ApiOperation("添加课程基础信息")
    @PostMapping("/addCourseInfo")
    public CommonResult addCourseInfo(@RequestBody CourseInfoVo courseInfoVo) {
        //返回添加之后课程id，为了后面添加大纲使用
        String id = courseService.saveCourseInfo(courseInfoVo);
        return CommonResult.ok().data("courseId",id);
    }

    @ApiOperation("修改课程基础信息")
    @PutMapping("/updateCourseInfo")
    @CacheEvict(value = "indexCourse",key = "1")
    public CommonResult updateCourseInfo(@RequestBody CourseInfoVo courseInfoVo) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag=courseService.updateCourseInfo(courseInfoVo);
        return CommonResult.ok().data("courseId",courseInfoVo.getId());
    }


    @ApiOperation("分页课程基础信息")
    @PostMapping("/courseLimit")
    public CommonResult courseLimit(@RequestBody CourseQuery courseQuery) {

        Page<Course> page = new Page<>(courseQuery.getCurrent(), courseQuery.getLimit());
        courseService.pageQuery(page,courseQuery);
        long total = page.getTotal();
        List<Course> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("根据id查询课程基础信息")
    @PostMapping("/getCourseById/{id}")
    public CommonResult getCourseById(@PathVariable String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        CourseInfoVo courseInfoVo= courseService.getCourseInfoById(id);
        return CommonResult.ok().data("course",courseInfoVo);
    }


    @ApiOperation("根据id发布课程")
    @PostMapping("/publishCourse/{id}")
    @CacheEvict(value = "indexCourse",key = "1")
    public CommonResult publishCourse(@PathVariable String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag= courseService.publishCourseById(id);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @ApiOperation("根据id发布课程")
    @PostMapping("/deleteCourse/{id}")
    @CacheEvict(value = "indexCourse",key = "1")
    public CommonResult deleteCourse(@PathVariable String id) {
        //返回添加之后课程id，为了后面添加大纲使用
        boolean flag= courseService.removeCourse(id);
        return flag?CommonResult.ok():CommonResult.error();
    }

    @ApiOperation(value = "判断用户是否收藏课程")
    @PostMapping("getUserCourse")
    public CommonResult getUserCourse(@RequestBody UserCourse userCourse) {

        if (StringUtils.isEmpty(userCourse.getUserId())) {
            return CommonResult.ok().data("data", 0);
        } else {
            QueryWrapper<UserCourse> userCourseQueryWrapper = new QueryWrapper<>();
            Map<String, Object> map = new HashMap<>(2);
            map.put("user_id", userCourse.getUserId());
            map.put("course_id", userCourse.getCourseId());
            int userCount = userCourseService.count(userCourseQueryWrapper.allEq(map));
                return userCount>0?CommonResult.ok().data("data", 1): CommonResult.ok().data("data", 0);
        }
    }


    @ApiOperation(value = "收藏")
    @PostMapping("changeUserCourse")
    public CommonResult changeUserCourse(@RequestBody UserCourse userCourse) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("user_id", userCourse.getUserId());
        map.put("course_id", userCourse.getCourseId());
        QueryWrapper<UserCourse> userCourseQueryWrapper = new QueryWrapper<>();
        if (userCourseService.count(userCourseQueryWrapper.allEq(map))==0){
            boolean flag = userCourseService.save(userCourse);
            return flag?CommonResult.ok().data("data",1):CommonResult.ok().data("data",0);
        }else {
            boolean flag = userCourseService.remove(userCourseQueryWrapper.allEq(map));
            return flag?CommonResult.ok().data("data",0):CommonResult.ok().data("data",1);
        }
    }
}

