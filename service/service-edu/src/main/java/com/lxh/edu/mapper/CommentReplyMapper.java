package com.lxh.edu.mapper;

import com.lxh.edu.entity.CommentReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 回复 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface CommentReplyMapper extends BaseMapper<CommentReply> {

}
