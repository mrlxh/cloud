package com.lxh.edu.entity.vo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Lee
 * @date 2020/5/1 -22:36
 */
@Data
public class ArticleQueryObject extends QueryObject {

    @ApiModelProperty(value = "文章标题")
    private String title;

    @ApiModelProperty(value = "发布时间-开始")
    private Date begin;

    @ApiModelProperty(value = "发布时间-结束")
    private Date end;
}
