package com.lxh.edu.mapper;

import com.lxh.edu.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-18
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
