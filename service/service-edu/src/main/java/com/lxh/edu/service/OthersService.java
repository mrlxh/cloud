package com.lxh.edu.service;

import com.lxh.serviceothers.entity.Others;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 创意工具 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface OthersService extends IService<Others> {

}
