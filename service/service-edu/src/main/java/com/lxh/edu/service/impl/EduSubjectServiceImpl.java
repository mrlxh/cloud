package com.lxh.edu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.cloud.common.servicebase.tree.TreeUtils;
import com.lxh.edu.entity.EduSubject;
import com.lxh.edu.entity.excel.SubjectData;
import com.lxh.edu.listener.SubjectExcelListener;
import com.lxh.edu.mapper.EduSubjectMapper;
import com.lxh.edu.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-09
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public List<BaseTreeGrid> subjectTree() {
        return TreeUtils.formatTree(baseMapper.subjectTree(),true);
    }

    //添加课程分类
    @Override
    public boolean saveSubject(MultipartFile file,EduSubjectService subjectService) {
        try {
            //文件输入流
            InputStream in = file.getInputStream();
            //调用方法进行读取
            EasyExcel.read(in, SubjectData.class,new SubjectExcelListener(subjectService)).sheet().doRead();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
