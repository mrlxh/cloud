package com.lxh.edu.service;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.edu.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-09
 */
public interface EduSubjectService extends IService<EduSubject> {

    List<BaseTreeGrid> subjectTree();

    boolean saveSubject(MultipartFile file,EduSubjectService subjectService);
}
