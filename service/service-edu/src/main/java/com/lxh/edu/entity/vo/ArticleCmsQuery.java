package com.lxh.edu.entity.vo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/14 -11:54
 */
@Data
public class ArticleCmsQuery extends QueryObject {

    private String title;
    private String subjectParentId;
    private String subjectId;
}
