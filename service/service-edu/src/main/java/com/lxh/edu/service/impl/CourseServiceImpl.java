package com.lxh.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.edu.entity.Chapter;
import com.lxh.edu.entity.Course;
import com.lxh.edu.entity.CourseDescription;
import com.lxh.edu.entity.Video;
import com.lxh.edu.entity.vo.CourseCmsQuery;
import com.lxh.edu.entity.vo.CourseInfoVo;
import com.lxh.edu.entity.vo.CourseQuery;
import com.lxh.edu.mapper.CourseMapper;
import com.lxh.edu.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@Service

public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

    @Autowired
    private CourseDescriptionService descriptionService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private VodService vodService;

    @Autowired
    private VideoService videoService;

    @Transactional
    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        Course course = new Course();
        BeanUtils.copyProperties(courseInfoVo, course);
        baseMapper.insert(course);
        CourseDescription courseDescription = new CourseDescription();
        courseDescription.setId(course.getId());
        courseDescription.setDescription(courseInfoVo.getDescription());
        descriptionService.save(courseDescription);
        return course.getId();
    }

    @Override
    public CourseInfoVo getCourseInfoById(String id) {
        CourseInfoVo courseInfoVo = baseMapper.getCourseInfoById(id);
        return courseInfoVo;
    }

    @Override
    public boolean updateCourseInfo(CourseInfoVo courseInfoVo) {
        int flag = baseMapper.updateCourseInfo(courseInfoVo);
        return flag != 0;
    }

    @Override
    public boolean publishCourseById(String id) {
        int flag = baseMapper.publishCourseById(id);
        return flag != 0;
    }

    @Override
    public void pageQuery(Page<Course> page, CourseQuery courseQuery) {

        QueryWrapper<Course> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("gmt_create");
        if (!StringUtils.isEmpty(courseQuery.getTitle())) {
            wrapper.like("title", courseQuery.getTitle());
        }
        if (!StringUtils.isEmpty(courseQuery.getStatus())) {
            wrapper.eq("status", courseQuery.getStatus());
        }
        baseMapper.selectPage(page, wrapper);
    }

    @Override
    public void pageQuery(Page<Course> page, CourseCmsQuery courseCmsQuery) {
        QueryWrapper<Course> wrapper = new QueryWrapper<>();

        if (courseCmsQuery.getFree()!=-1){
            if (courseCmsQuery.getFree() == 0) {
                wrapper.eq("price", 0);
            } else {
                wrapper.ne("price", 0);
            }
        }

        if (courseCmsQuery.getType()!=0){
            if (courseCmsQuery.getType() == 1) {
                wrapper.orderByDesc("view_count");
            } else {
                wrapper.orderByDesc("gmt_modified");
            }
        }

        if (!"1".equals(courseCmsQuery.getSubjectParentId())) {
            wrapper.eq("subject_parent_id", courseCmsQuery.getSubjectParentId());
        }

        if (!"1".equals(courseCmsQuery.getSubjectId())){
            wrapper.eq("subject_id", courseCmsQuery.getSubjectId());
        }
        baseMapper.selectPage(page, wrapper);
    }

    @Transactional
    @Override
    public boolean removeCourse(String id) {
        baseMapper.deleteById(id);
        QueryWrapper<Chapter> chapterQueryWrapper = new QueryWrapper<>();
        chapterService.remove(chapterQueryWrapper.eq("course_id",id));
        QueryWrapper<Video> videoQueryWrapper = new QueryWrapper<>();
        List<Map<String, Object>> list = videoService.listMaps(videoQueryWrapper.select("video_source_id").eq("course_id", id));
        if (list.size()>0){
            String ids="";
            for (Map<String, Object> map : list) {
                ids+=","+map.get("videoSourceId");
            }
            vodService.deleteAlyiVideo(id.substring(1));
        }
        videoService.remove(videoQueryWrapper.eq("course_id", id));
        return true;
    }
}
