package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.ArticleLike;
import com.lxh.edu.entity.UserArticle;
import com.lxh.edu.entity.vo.UserArticleLikeVo;
import com.lxh.edu.service.ArticleLikeService;
import com.lxh.edu.service.UserArticleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 点赞 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@RestController
@RequestMapping("/eduservice/articleLike")
public class ArticleLikeController {

    @Autowired
    private ArticleLikeService articleLikeService;

    @Autowired
    private UserArticleService userArticleService;

    @ApiOperation(value = "判断用户是否点赞，是否收藏，获取文章的点赞数量")
    @PostMapping("getUserArticleLike")
    public CommonResult getUserArticleLike(@RequestBody UserArticleLikeVo userArticleLikeVo) {

        QueryWrapper<ArticleLike> articleLikeQueryWrapper = new QueryWrapper<>();
        int count = articleLikeService.count(articleLikeQueryWrapper.eq("article_id", userArticleLikeVo.getArticleId()));
        if (StringUtils.isEmpty(userArticleLikeVo.getUserId())) {
            return CommonResult.ok().data("count", count).data("userCount", 0).data("userLike", 0);
        } else {
            QueryWrapper<ArticleLike> userArticleLikeQueryWrapper = new QueryWrapper<>();
            Map<String, Object> map = new HashMap<>(2);
            map.put("user_id", userArticleLikeVo.getUserId());
            map.put("article_id", userArticleLikeVo.getArticleId());
            int userCount = articleLikeService.count(userArticleLikeQueryWrapper.allEq(map));

            QueryWrapper<UserArticle> userArticleQueryWrapper = new QueryWrapper<>();
            int userLike = userArticleService.count(userArticleQueryWrapper.allEq(map));

            if (userCount>0){
                return userLike>0?CommonResult.ok().data("count", count).data("userCount", userCount).data("userLike", userLike):
                        CommonResult.ok().data("count", count).data("userCount", userCount).data("userLike", 0);
            }else {
                return userLike>0?CommonResult.ok().data("count", count).data("userCount", 0).data("userLike", userLike):
                        CommonResult.ok().data("count", count).data("userCount", 0).data("userLike", 0);
            }
        }
    }


    @ApiOperation(value = "收藏")
    @PostMapping("changeUserArticle")
    public CommonResult changeUserArticle(@RequestBody UserArticleLikeVo userArticleLikeVo) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("user_id", userArticleLikeVo.getUserId());
        map.put("article_id", userArticleLikeVo.getArticleId());
        QueryWrapper<UserArticle> userArticleQueryWrapper = new QueryWrapper<>();
        if (userArticleService.count(userArticleQueryWrapper.allEq(map))==0){
            UserArticle userArticle = new UserArticle();
            userArticle.setUserId(userArticleLikeVo.getUserId());
            userArticle.setArticleId(userArticleLikeVo.getArticleId());
            boolean flag = userArticleService.save(userArticle);
            return flag?CommonResult.ok().data("data",1):CommonResult.ok().data("data",0);
        }else {
            boolean flag = userArticleService.remove(userArticleQueryWrapper.allEq(map));
            return flag?CommonResult.ok().data("data",0):CommonResult.ok().data("data",1);
        }
    }
    @ApiOperation(value = "点赞")
    @PostMapping("changeUserArticleLike")
    public CommonResult changeUserArticleLike(@RequestBody UserArticleLikeVo userArticleLikeVo) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("user_id", userArticleLikeVo.getUserId());
        map.put("article_id", userArticleLikeVo.getArticleId());
        QueryWrapper<ArticleLike> userArticleQueryWrapper = new QueryWrapper<>();
        if (articleLikeService.count(userArticleQueryWrapper.allEq(map))==0){
            ArticleLike articleLike = new ArticleLike();
            articleLike.setUserId(userArticleLikeVo.getUserId());
            articleLike.setArticleId(userArticleLikeVo.getArticleId());
            boolean flag = articleLikeService.save(articleLike);
            return flag?CommonResult.ok().data("data",1):CommonResult.ok().data("data",0);
        }else {
            boolean flag = articleLikeService.remove(userArticleQueryWrapper.allEq(map));
            return flag?CommonResult.ok().data("data",0):CommonResult.ok().data("data",1);
        }

    }
}

