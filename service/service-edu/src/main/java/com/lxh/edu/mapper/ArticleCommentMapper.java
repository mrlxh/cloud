package com.lxh.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.edu.entity.ArticleComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface ArticleCommentMapper extends BaseMapper<ArticleComment> {

    List<Map> getComment(@Param("id") String id);
}
