package com.lxh.edu.service;

import com.lxh.edu.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.edu.entity.vo.VideoInfoVo;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface VideoService extends IService<Video> {

    boolean updateVideoInfo(VideoInfoVo video);
}
