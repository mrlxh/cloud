package com.lxh.edu.entity.vo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/24 -20:57
 */
@Data
public class PersonalVo extends QueryObject {
    private String id;
}
