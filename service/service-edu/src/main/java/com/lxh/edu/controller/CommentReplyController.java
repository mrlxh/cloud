package com.lxh.edu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 回复 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@RestController
@RequestMapping("/edu/comment-reply")
public class CommentReplyController {

}

