package com.lxh.edu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.edu.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.edu.entity.vo.CourseCmsQuery;
import com.lxh.edu.entity.vo.CourseInfoVo;
import com.lxh.edu.entity.vo.CourseQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface CourseService extends IService<Course> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfoById(String id);

    boolean updateCourseInfo(CourseInfoVo courseInfoVo);

    boolean publishCourseById(String id);

    void pageQuery(Page<Course> page, CourseQuery courseQuery);

    void pageQuery(Page<Course> page, CourseCmsQuery courseCmsQuery);

    boolean removeCourse(String id);
}
