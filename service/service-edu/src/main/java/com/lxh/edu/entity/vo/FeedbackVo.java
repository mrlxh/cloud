package com.lxh.edu.entity.vo;

import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/24 -20:57
 */
@Data
public class FeedbackVo{    private String id;
    private String content;
}
