package com.lxh.edu.entity.tree;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import lombok.Data;

/**
 * @author Lee
 * @date 2020/5/2 -13:10
 */
@Data
public class ChapterTreeGrid extends BaseTreeGrid {

    private String videoSourceId;
}
