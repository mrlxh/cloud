package com.lxh.edu.service.impl;

import com.lxh.edu.entity.UserCourse;
import com.lxh.edu.mapper.UserCourseMapper;
import com.lxh.edu.service.UserCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
@Service
public class UserCourseServiceImpl extends ServiceImpl<UserCourseMapper, UserCourse> implements UserCourseService {

}
