package com.lxh.edu.service.impl;

import com.lxh.cloud.common.servicebase.tree.TreeUtils;
import com.lxh.edu.entity.ArticleComment;
import com.lxh.edu.entity.CommentTreeGrid;
import com.lxh.edu.mapper.ArticleCommentMapper;
import com.lxh.edu.service.ArticleCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@Service
public class ArticleCommentServiceImpl extends ServiceImpl<ArticleCommentMapper, ArticleComment> implements ArticleCommentService {

    @Override
    public List<Map> getComment(String id) {

        return baseMapper.getComment(id);
    }
}
