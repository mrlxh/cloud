package com.lxh.edu.service;

import com.lxh.edu.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 反馈 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
public interface FeedbackService extends IService<Feedback> {

}
