package com.lxh.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.cloud.common.servicebase.tree.TreeUtils;
import com.lxh.edu.entity.Chapter;
import com.lxh.edu.entity.tree.ChapterTreeGrid;
import com.lxh.edu.mapper.ChapterMapper;
import com.lxh.edu.service.ChapterService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {

    @Override
    public List<ChapterTreeGrid> getChapter(String courseId) {
        List<ChapterTreeGrid> allChapter = baseMapper.getAllByCourseId(courseId);
        return TreeUtils.formatTree(allChapter,true);
    }
}
