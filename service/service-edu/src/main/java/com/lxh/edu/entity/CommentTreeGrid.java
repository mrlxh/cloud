package com.lxh.edu.entity;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import lombok.Data;

import java.util.Date;

/**
 * @author Lee
 * @date 2020/4/25 -15:37
 */
@Data
public class CommentTreeGrid extends BaseTreeGrid {

    private String content;
    private Date gmtCreate;
}
