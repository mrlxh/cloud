package com.lxh.edu.mapper;

import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.edu.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.edu.entity.tree.ChapterTreeGrid;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface ChapterMapper extends BaseMapper<Chapter> {

    List<ChapterTreeGrid> getAllByCourseId(@Param("courseId") String courseId);

}
