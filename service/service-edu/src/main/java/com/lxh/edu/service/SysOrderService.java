package com.lxh.edu.service;

import com.lxh.edu.entity.SysOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程收藏 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-05-16
 */
public interface SysOrderService extends IService<SysOrder> {

}
