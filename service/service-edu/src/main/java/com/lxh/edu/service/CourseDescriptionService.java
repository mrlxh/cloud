package com.lxh.edu.service;

import com.lxh.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface CourseDescriptionService extends IService<CourseDescription> {

}
