package com.lxh.edu.mapper;

import com.lxh.serviceothers.entity.Others;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 创意工具 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface OthersMapper extends BaseMapper<Others> {

}
