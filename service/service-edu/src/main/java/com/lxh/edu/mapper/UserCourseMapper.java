package com.lxh.edu.mapper;

import com.lxh.edu.entity.UserCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收藏 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
public interface UserCourseMapper extends BaseMapper<UserCourse> {

}
