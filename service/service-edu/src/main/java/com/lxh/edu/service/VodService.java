package com.lxh.edu.service;

import com.lxh.common.cmomonutils.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Lee
 * @date 2020/4/26 -16:49
 */
@FeignClient(value = "service-vod")
public interface VodService {

    //上传视频到阿里云
    @DeleteMapping("/vodservice/video/deleteAlyiVideo/{id}")
    public CommonResult deleteAlyiVideo(@PathVariable("id") String id);
}
