package com.lxh.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.edu.mapper.OthersMapper;
import com.lxh.edu.service.OthersService;
import com.lxh.serviceothers.entity.Others;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 创意工具 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@Service
public class OthersServiceImpl extends ServiceImpl<OthersMapper, Others> implements OthersService {

}
