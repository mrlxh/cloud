package com.lxh.edu.entity.vo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/13 -9:38
 */
@Data
public class CourseQuery  extends QueryObject {

    private String title;

    private String status;


}
