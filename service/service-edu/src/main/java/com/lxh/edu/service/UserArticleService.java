package com.lxh.edu.service;

import com.lxh.edu.entity.UserArticle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
public interface UserArticleService extends IService<UserArticle> {

}
