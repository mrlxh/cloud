package com.lxh.edu.service.impl;

import com.lxh.edu.entity.UserArticle;
import com.lxh.edu.mapper.UserArticleMapper;
import com.lxh.edu.service.UserArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
@Service
public class UserArticleServiceImpl extends ServiceImpl<UserArticleMapper, UserArticle> implements UserArticleService {

}
