package com.lxh.edu.service.impl;

import com.lxh.edu.entity.CommentReply;
import com.lxh.edu.mapper.CommentReplyMapper;
import com.lxh.edu.service.CommentReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 回复 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
@Service
public class CommentReplyServiceImpl extends ServiceImpl<CommentReplyMapper, CommentReply> implements CommentReplyService {

}
