package com.lxh.edu.mapper;

import com.lxh.edu.entity.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.edu.entity.vo.VideoInfoVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface VideoMapper extends BaseMapper<Video> {

    int updateVideoInfo(@Param("video") VideoInfoVo video);
}
