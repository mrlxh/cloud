package com.lxh.edu.entity.subject;

import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/10 -10:45
 */
@Data
public class TwoSubject {
    private String id;
    private String title;
}
