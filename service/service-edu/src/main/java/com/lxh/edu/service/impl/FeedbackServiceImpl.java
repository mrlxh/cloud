package com.lxh.edu.service.impl;

import com.lxh.edu.entity.Feedback;
import com.lxh.edu.mapper.FeedbackMapper;
import com.lxh.edu.service.FeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 反馈 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-24
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements FeedbackService {

}
