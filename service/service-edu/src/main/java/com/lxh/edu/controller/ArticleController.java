package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.Article;
import com.lxh.edu.entity.EduSubject;
import com.lxh.edu.entity.vo.ArticleCmsQuery;
import com.lxh.edu.entity.vo.ArticleQueryObject;
import com.lxh.edu.service.ArticleService;
import com.lxh.edu.service.EduSubjectService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 文章 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-18
 */
@RestController
@RequestMapping("/eduservice/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private EduSubjectService eduSubjectService;


    @ApiOperation(value = "前台获取热门文章前6条数据")
    @PostMapping("getHotArticle")
    public CommonResult getHotArticle() {
        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();

        return CommonResult.ok().data("hotArticleList", articleService.list(articleQueryWrapper.orderByDesc("view_count").last("limit 0,6")));
    }


    @ApiOperation(value = "前台获取文章数据")
    @PostMapping("getArticleList")
    public CommonResult getHotArticle(@RequestBody ArticleCmsQuery articleCmsQuery) {

        Page<Article> page = new Page<>(articleCmsQuery.getCurrent(), articleCmsQuery.getLimit());
        articleService.pageQuery(page,articleCmsQuery);
        return CommonResult.ok().data("total",page.getTotal()).data("rows",page.getRecords());
    }

    @ApiOperation(value = "前台点击文章")
    @PostMapping("getArticleCms/{id}")
    public CommonResult getArticleCms(@PathVariable("id") String id) {

        Article article = articleService.getById(id);
        article.setViewCount(article.getViewCount()+1);
        articleService.updateById(article);
        String subjectId = article.getSubjectId();
        String subjectParentId = article.getSubjectParentId();
        EduSubject parentSubject = eduSubjectService.getById(subjectParentId);
        EduSubject subject = eduSubjectService.getById(subjectId);
        return CommonResult.ok().data("article",article).data("parentTitle",parentSubject.getTitle()).data("subjectTitle",subject.getTitle());
    }


    @ApiOperation(value = "分页查询文章信息")
    @PostMapping("/findArticles")
    public CommonResult pageUser(
            @ApiParam(name = "articleQueryObject", value = "查询文章对象", required = true)
            @RequestBody ArticleQueryObject articleQueryObject) {
        Page<Article> page = new Page<>(articleQueryObject.getCurrent(), articleQueryObject.getLimit());
        articleService.pageQuery(page,articleQueryObject);
        long total = page.getTotal();
        List<Article> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation(value = "删除文章信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "用户id", required = true)
            @PathVariable String id) {

        boolean flag = articleService.removeById(id);

        return flag ? CommonResult.ok() : CommonResult.error();
    }



    @ApiOperation(value = "修改文章信息")
    @PutMapping("/update/{id}")
    public CommonResult update(
            @ApiParam(name = "id", value = "用户id", required = true)
            @PathVariable String id,
            @ApiParam(name = "Article", value = "文章对象", required = true)
            @RequestBody Article eduArticle) {
        eduArticle.setId(id);
        System.out.println(eduArticle);
        boolean flag = articleService.updateById(eduArticle);
        return flag ? CommonResult.ok() : CommonResult.error();
    }


    @ApiOperation(value = "添加文章信息")
    @PostMapping("/insert")
    public CommonResult insert(
            @ApiParam(name = "eduArticle", value = "文章对象", required = true)
            @RequestBody Article eduArticle) {
        boolean flag = articleService.save(eduArticle);
        return flag ? CommonResult.ok() : CommonResult.error();
    }



}

