package com.lxh.edu.mapper;

import com.lxh.edu.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.edu.entity.vo.CourseInfoVo;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
public interface CourseMapper extends BaseMapper<Course> {

    CourseInfoVo getCourseInfoById(@Param("id") String id);

    int updateCourseInfo(@Param("courseInfoVo") CourseInfoVo courseInfoVo);

    int publishCourseById(@Param("id") String id);
}
