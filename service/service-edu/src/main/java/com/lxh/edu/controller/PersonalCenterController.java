package com.lxh.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.*;
import com.lxh.edu.entity.vo.FeedbackVo;
import com.lxh.edu.entity.vo.PersonalVo;
import com.lxh.edu.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2020/4/24 -20:45
 */
@RestController
@RequestMapping("/eduservice/personalCenter")
public class PersonalCenterController {

    @Autowired
    private UserCourseService userCourseService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserArticleService userArticleService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private FeedbackService feedbackService;

    @ApiOperation("个人中心查询收藏课程")
    @PostMapping("/getPersonalCourse")
    public CommonResult getPersonalCourse(@RequestBody PersonalVo personalVo){
        //先查出课程id
        QueryWrapper<UserCourse> wrapper = new QueryWrapper<>();
        wrapper.select("course_id").eq("user_id",personalVo.getId());
        List<Map<String, Object>> list = userCourseService.listMaps(wrapper);
        if (list.size()==0){
            return CommonResult.ok().data("total",0).data("rows", Collections.EMPTY_LIST);
        }
        List<String> courseIdList=new ArrayList<>(list.size());
        for (Map<String, Object> map : list) {
            courseIdList.add((String) map.get("course_id"));
        }
        QueryWrapper<Course> courseWrapper = new QueryWrapper<>();
        courseWrapper.in("id",courseIdList);
        Page<Course> page = new Page<>(personalVo.getCurrent(), personalVo.getLimit());
        courseService.page(page,courseWrapper);
        return CommonResult.ok().data("total",page.getTotal()).data("rows",page.getRecords());
    }


    @ApiOperation("个人中心查询收藏文章")
    @PostMapping("/getPersonalArticle")
    public CommonResult getPersonalArticle(@RequestBody PersonalVo personalVo){
        //先查出课程id
        QueryWrapper<UserArticle> wrapper = new QueryWrapper<>();
        wrapper.select("article_id").eq("user_id",personalVo.getId());
        List<Map<String, Object>> list = userArticleService.listMaps(wrapper);
        if (list.size()==0){
            return CommonResult.ok().data("total",0).data("rows", Collections.EMPTY_LIST);
        }
        List<String> articleIdList=new ArrayList<>(list.size());
        for (Map<String, Object> map : list) {
            articleIdList.add((String) map.get("article_id"));
        }
        QueryWrapper<Article> courseWrapper = new QueryWrapper<>();
        courseWrapper.in("id",articleIdList);
        Page<Article> page = new Page<>(personalVo.getCurrent(), personalVo.getLimit());
        articleService.page(page,courseWrapper);
        return CommonResult.ok().data("total",page.getTotal()).data("rows",page.getRecords());
    }


    @ApiOperation("反馈")
    @PostMapping("/addFeedback")
    public CommonResult addFeedback(@RequestBody FeedbackVo feedbackVo){
        //先查出课程id
        Feedback feedback = new Feedback();
        feedback.setUserId(feedbackVo.getId());
        feedback.setContent(feedbackVo.getContent());
        boolean save = feedbackService.save(feedback);
        return save?CommonResult.ok():CommonResult.error();
    }

}
