package com.lxh.edu.service.impl;

import com.lxh.edu.entity.CourseDescription;
import com.lxh.edu.mapper.CourseDescriptionMapper;
import com.lxh.edu.service.CourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-10
 */
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionMapper, CourseDescription> implements CourseDescriptionService {

}
