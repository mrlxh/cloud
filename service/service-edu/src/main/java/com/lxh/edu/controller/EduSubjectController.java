package com.lxh.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxh.cloud.common.servicebase.exceptionhandler.MyException;
import com.lxh.cloud.common.servicebase.tree.BaseTreeGrid;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.entity.EduSubject;
import com.lxh.edu.service.EduSubjectService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-09
 */
@RestController
@RequestMapping("/eduservice/class")
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    @ApiOperation(value = "所有分类信息")
    @PostMapping("/tree")
    public CommonResult tree() {
        List<BaseTreeGrid> list = eduSubjectService.subjectTree();
        return CommonResult.ok().data("tree", list);
    }

    @ApiOperation(value = "添加分类信息")
    @PostMapping("/addClass")
    public CommonResult addClass(
            @ApiParam(name = "eduSubject", value = "列表对象", required = true)
            @RequestBody EduSubject eduSubject) {
        boolean flag = eduSubjectService.save(eduSubject);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "导入加分类信息")
    @PostMapping("/importClass")
    public CommonResult importClass(
            @ApiParam(name = "file", value = "excel文件", required = true)
                    MultipartFile file) {
        if (file.isEmpty()){
            throw  new MyException(20001,"上传文件不能为空");
        }
        boolean flag = eduSubjectService.saveSubject(file,eduSubjectService);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "修改分类信息")
    @PutMapping("/updateClass")
    public CommonResult updateClass(
            @ApiParam(name = "eduSubject", value = "列表对象", required = true)
            @RequestBody EduSubject eduSubject) {
        boolean flag = eduSubjectService.updateById(eduSubject);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "删除分类信息")
    @DeleteMapping("/deleteClass/{id}")
    public CommonResult deleteClass(
            @ApiParam(name = "id", value = "用户id", required = true)
            @PathVariable String id) {
        EduSubject subject = eduSubjectService.getById(id);
        boolean flag;
        if ("1".equals(subject.getParentId())) {
            QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
            wrapper.eq("parent_id", id).or().eq("id",id);
            flag = eduSubjectService.remove(wrapper);
        } else {
            flag = eduSubjectService.removeById(id);
        }
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "查询该id的下级课程")
    @PostMapping("/selectOneSubject/{id}")
    public CommonResult selectOneSubject(
            @ApiParam(name = "id", value = "分类Id", required = true)
            @PathVariable String id) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.select("id","title").eq("parent_id",id);
        List<EduSubject> subjectList = eduSubjectService.list(wrapper);
        return  CommonResult.ok().data("data",subjectList);
    }
}

