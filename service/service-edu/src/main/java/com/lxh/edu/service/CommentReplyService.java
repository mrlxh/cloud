package com.lxh.edu.service;

import com.lxh.edu.entity.CommentReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 回复 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface CommentReplyService extends IService<CommentReply> {

}
