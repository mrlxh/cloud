package com.lxh.edu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.edu.service.OthersService;
import com.lxh.serviceothers.entity.Others;
import com.lxh.serviceothers.qo.OthersQuery;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 创意工具 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@RestController
@RequestMapping("/eduservice/others")
public class OthersController {

    @Autowired
    OthersService othersService;

    @ApiOperation(value = "分页查询创意工具信息")
    @PostMapping("/pageOthers")
//    @RequestMapping(value = "/pageUser",method = RequestMethod.PUT)
    public CommonResult pageOthers(
            @ApiParam(name = "OthersQuery", value = "查询创意工具对象", required = true)
            @RequestBody OthersQuery othersQuery) {

        Page<Others> page = new Page<>(othersQuery.getCurrent(), othersQuery.getLimit());

        QueryWrapper<Others> wrapper = new QueryWrapper<>();

        wrapper.orderByDesc("gmt_create");
        if (StringUtils.isEmpty(othersQuery.getTitle())){
        }else {
            //模糊查询
            wrapper.like("title",othersQuery.getTitle());
        }
        othersService.page(page,wrapper);

        long total = page.getTotal();
        List<Others> records = page.getRecords();

        return CommonResult.ok().data("total", total).data("rows", records);
    }

    //前端创意工具
    @ApiOperation(value = "前端查询")
    @PostMapping("/selectOther/{type}")
//    @RequestMapping(value = "/pageUser",method = RequestMethod.PUT)
    public CommonResult selectOther(@PathVariable String type) {

        QueryWrapper<Others> wrapper = new QueryWrapper<>();

        wrapper.eq("type",Integer.parseInt(type));
        List<Others> list = othersService.list(wrapper);

        return CommonResult.ok().data("rows", list);
    }

    @ApiOperation(value = "删除创意工具信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "ID", required = true)
            @PathVariable String id) {

        boolean flag = othersService.removeById(id);

        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "修改创意工具信息")
    @PutMapping("/modify/{id}")
    public CommonResult modify(
            @ApiParam(name = "id", value = "ID", required = true)
            @PathVariable String id,
            @ApiParam(name = "others", value = "创意工具对象", required = true)
            @RequestBody Others others) {
        others.setId(id);
        //按di更新（按id修改）
        boolean flag = othersService.updateById(others);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "添加创意工具信息")
    @PostMapping("/addOthers")
    public CommonResult addOthers(
            @ApiParam(name = "others", value = "创意工具对象", required = true)
            @RequestBody Others others) {

        boolean flag = othersService.save(others);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

}

