package com.lxh.edu.service;

import com.lxh.edu.entity.ArticleComment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.edu.entity.CommentTreeGrid;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-25
 */
public interface ArticleCommentService extends IService<ArticleComment> {

    List<Map> getComment(String id);
}
