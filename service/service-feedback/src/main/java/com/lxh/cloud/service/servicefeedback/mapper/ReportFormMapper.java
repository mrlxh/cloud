package com.lxh.cloud.service.servicefeedback.mapper;

import com.lxh.cloud.service.servicefeedback.entity.ReportForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-20
 */
public interface ReportFormMapper extends BaseMapper<ReportForm> {

    int countLoginNum(@Param("day") String day);
}
