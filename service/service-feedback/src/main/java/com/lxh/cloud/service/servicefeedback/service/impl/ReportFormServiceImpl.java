package com.lxh.cloud.service.servicefeedback.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lxh.cloud.service.servicefeedback.client.UcenterClient;
import com.lxh.cloud.service.servicefeedback.entity.ReportForm;
import com.lxh.cloud.service.servicefeedback.mapper.ReportFormMapper;
import com.lxh.cloud.service.servicefeedback.service.ReportFormService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.common.cmomonutils.CommonResult;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-20
 */
@Service
public class ReportFormServiceImpl extends ServiceImpl<ReportFormMapper, ReportForm> implements ReportFormService {

    @Autowired
    private UcenterClient ucenterClient;

    @Override
    public void registerCount(String day) {

        //添加记录之前删除表相同日期的数据
        QueryWrapper<ReportForm> wrapper = new QueryWrapper<>();
        wrapper.eq("date_tatol",day);
        baseMapper.delete(wrapper);

        //远程调用得到某一天注册人数
        CommonResult registerR = ucenterClient.countRegister(day);
        Integer countRegister = (Integer)registerR.getData().get("countRegister");

        //把获取数据添加数据库，统计分析表里面
        ReportForm sta = new ReportForm();
        sta.setRegisterNum(countRegister); //注册人数
        sta.setDateTatol(day);//统计日期

        //获取每天的登录人数
        int loginCount=baseMapper.countLoginNum(day);

        sta.setVideViewNum(RandomUtils.nextInt(100,200));
        sta.setLoginNum(loginCount);
        sta.setCourseNum(RandomUtils.nextInt(100,200));
        baseMapper.insert(sta);
    }

    //图表显示，返回两部分数据，日期json数组，数量json数组
    @Override
    public Map<String, Object> getShowData(String type, String begin, String end) {
        //根据条件查询对应数据
        QueryWrapper<ReportForm> wrapper = new QueryWrapper<>();
        wrapper.between("date_tatol",begin,end);
        wrapper.select("date_tatol",type);
        wrapper.orderByAsc("date_tatol");
        List<ReportForm> staList = baseMapper.selectList(wrapper);

        //因为返回有两部分数据：日期 和 日期对应数量
        //前端要求数组json结构，对应后端java代码是list集合
        //创建两个list集合，一个日期list，一个数量list
        List<String> date_calculatedList = new ArrayList<>();
        List<Integer> numDataList = new ArrayList<>();

        //遍历查询所有数据list集合，进行封装
        for (int i = 0; i < staList.size(); i++) {
            ReportForm daily = staList.get(i);
            //封装日期list集合
            date_calculatedList.add(daily.getDateTatol());
            //封装对应数量
            switch (type) {
                case "login_num":
                    numDataList.add(daily.getLoginNum());
                    break;
                case "register_num":
                    numDataList.add(daily.getRegisterNum());
                    break;
                case "video_view_num":
                    numDataList.add(daily.getVideViewNum());
                    break;
                case "course_num":
                    numDataList.add(daily.getCourseNum());
                    break;
                default:
                    break;
            }
        }
        //把封装之后两个list集合放到map集合，进行返回
        Map<String, Object> map = new HashMap<>();
        map.put("date_calculatedList",date_calculatedList);
        map.put("numDataList",numDataList);
        return map;
    }
}
