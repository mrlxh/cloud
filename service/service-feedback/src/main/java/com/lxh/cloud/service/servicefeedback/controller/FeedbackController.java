package com.lxh.cloud.service.servicefeedback.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicefeedback.entity.Feedback;
import com.lxh.cloud.service.servicefeedback.qo.FeedbackQueryObject;
import com.lxh.cloud.service.servicefeedback.service.FeedbackService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 反馈 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-04-28
 */
@RestController
@RequestMapping("/servicefeedback/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @ApiOperation(value = "查询反馈信息")
    @PostMapping("/feedbackList")
    public CommonResult feedbackList(){
        List<Feedback> list = feedbackService.list(null);
        return CommonResult.ok().data("list",list);
    }

    @ApiOperation(value = "分页查询用户信息")
    @PostMapping("/pageFeedback")
    public CommonResult pageFeedback(
            @ApiParam(name = "FeedbackQueryObject", value = "查询用户对象", required = true)
            @RequestBody FeedbackQueryObject feedbackQueryObject) {
        Page<Feedback> page = new Page<>(feedbackQueryObject.getCurrent(),feedbackQueryObject.getLimit());
        QueryWrapper<Feedback> wrapper = new QueryWrapper<>();
        wrapper.like("content",feedbackQueryObject.getContent());
        feedbackService.page(page,wrapper);
        long total = page.getTotal();
        List<Feedback> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }


    @ApiOperation(value = "添加反馈信息")
    @PostMapping("/feedbackInsert")
    public CommonResult feedbackInsert(
            @ApiParam(name = "feedback", value = "添加反馈信息", required = true)
            @RequestBody Feedback feedback) {
        boolean flag = feedbackService.save(feedback);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "修改反馈信息")
    @PutMapping("/update/{id}")
    public CommonResult update(
            @ApiParam(name = "id", value = "反馈id", required = true)
            @PathVariable String id,
            @ApiParam(name = "feedback", value = "修改反馈信息", required = true)
            @RequestBody Feedback feedback) {
        feedback.setId(id);
        boolean flag = feedbackService.updateById(feedback);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "删除反馈信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "删除反馈信息", required = true)
            @PathVariable String id) {
        boolean flag = feedbackService.removeById(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }
}

