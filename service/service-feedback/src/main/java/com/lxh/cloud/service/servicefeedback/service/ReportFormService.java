package com.lxh.cloud.service.servicefeedback.service;

import com.lxh.cloud.service.servicefeedback.entity.ReportForm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-20
 */
public interface ReportFormService extends IService<ReportForm> {

    //统计某一天注册人数,生成统计数据
    void registerCount(String day);

    //图表显示，返回两部分数据，日期json数组，数量json数组
    Map<String, Object> getShowData(String type, String begin, String end);
}
