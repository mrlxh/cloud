package com.lxh.cloud.service.servicefeedback;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Lee
 * @date 2020/4/5 -16:28
 */
@MapperScan("com.lxh.cloud.service.servicefeedback.mapper")
@ComponentScan("com.lxh")
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@EnableScheduling
public class  ApplicationServiceFeedbackMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationServiceFeedbackMain.class , args) ;
    }
}
