package com.lxh.cloud.service.servicefeedback.controller;

import com.lxh.cloud.service.servicefeedback.service.ReportFormService;
import com.lxh.common.cmomonutils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-20
 */
@RestController
@RequestMapping("/servicefeedback/reportForm")
public class ReportFormController {
    @Autowired
    private ReportFormService reportFormService;

    //统计某一天注册人数,生成统计数据
    @PostMapping("registerCount/{day}")
    public CommonResult registerCount(@PathVariable String day) {
        reportFormService.registerCount(day);
        return CommonResult.ok();
    }

    //图表显示，返回两部分数据，日期json数组，数量json数组
    @GetMapping("showData/{type}/{begin}/{end}")
    public CommonResult showData(@PathVariable String type,@PathVariable String begin,
                      @PathVariable String end) {
        Map<String,Object> map = reportFormService.getShowData(type,begin,end);
        return CommonResult.ok().data(map);
    }
}

