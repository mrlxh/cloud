package com.lxh.cloud.service.servicefeedback.client;


import com.lxh.common.cmomonutils.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-user")
public interface UcenterClient {

    //查询某一天注册人数
    @GetMapping("/serviceuser/countRegister/{day}")
    public CommonResult countRegister(@PathVariable("day") String day);
}
