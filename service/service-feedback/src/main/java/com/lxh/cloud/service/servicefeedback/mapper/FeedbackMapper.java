package com.lxh.cloud.service.servicefeedback.mapper;

import com.lxh.cloud.service.servicefeedback.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 反馈 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-04-28
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

}
