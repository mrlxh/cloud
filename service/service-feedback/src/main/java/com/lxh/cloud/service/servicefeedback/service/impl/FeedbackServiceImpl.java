package com.lxh.cloud.service.servicefeedback.service.impl;

import com.lxh.cloud.service.servicefeedback.entity.Feedback;
import com.lxh.cloud.service.servicefeedback.mapper.FeedbackMapper;
import com.lxh.cloud.service.servicefeedback.service.FeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 反馈 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-04-28
 */
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements FeedbackService {


}
