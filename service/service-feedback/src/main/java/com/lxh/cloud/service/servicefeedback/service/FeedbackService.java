package com.lxh.cloud.service.servicefeedback.service;

import com.lxh.cloud.service.servicefeedback.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 反馈 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-04-28
 */
public interface FeedbackService extends IService<Feedback> {


}
