package com.lxh.cloud.service.servicefeedback.qo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author Lee
 * @date 2020/4/7 -17:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "反馈查询对象")
public class FeedbackQueryObject extends QueryObject{

    @ApiModelProperty(value = "反馈内容")
    private String content;

}
