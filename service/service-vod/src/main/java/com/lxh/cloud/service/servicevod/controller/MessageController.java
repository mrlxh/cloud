package com.lxh.cloud.service.servicevod.controller;

import com.lxh.cloud.service.servicevod.service.MsmService;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.common.cmomonutils.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @author Lee
 * @date 2020/4/15 -17:26
 */
@RestController
@RequestMapping("/vodservice/msm")
public class MessageController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    //发送短信的方法
    @PostMapping("send/{phone}")
    public CommonResult sendMsm(@PathVariable String phone) {
        //1 从redis获取手机信息是否还存在，如果存在说明60秒没过
        String limitPhone = redisTemplate.opsForValue().get("limit"+phone);
        if(!StringUtils.isEmpty(limitPhone)) {
            return CommonResult.error().message("获取验证码过于频繁");
        }
        //1 从redis获取验证码，如果获取到直接返回
        String code = redisTemplate.opsForValue().get(phone);
        if(!StringUtils.isEmpty(code)) {
            return CommonResult.ok();
        }
        //2 如果redis获取 不到，进行阿里云发送
        //生成随机值，传递阿里云进行发送
        code = RandomUtil.getFourBitRandom();
        Map<String,Object> param = new HashMap<>();
        param.put("code",code);
        //调用service发送短信的方法
        boolean isSend = msmService.send(param,phone);
        if(isSend) {
            //发送成功，把发送成功验证码放到redis里面
            //保存60秒，，60秒之内不许再点击
            redisTemplate.opsForValue().set("limit"+phone,phone,1, TimeUnit.MINUTES);
            //设置有效时间
            redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);
            return CommonResult.ok();
        } else {
            return CommonResult.error().message("短信发送失败");
        }
    }
}
