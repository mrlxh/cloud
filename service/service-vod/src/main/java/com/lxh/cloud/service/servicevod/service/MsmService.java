package com.lxh.cloud.service.servicevod.service;


import java.util.Map;

/**
 * @author Lee
 * @date 2020/4/15 -17:27
 */
public interface MsmService {

    boolean send(Map<String, Object> param, String phone);
}



