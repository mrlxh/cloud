package com.lxh.cloud.service.servicevod;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Lee
 * @date 2020/4/5 -16:28
 */
@ComponentScan("com.lxh")
@EnableDiscoveryClient
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class })
public class ApplicationServiceVodMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationServiceVodMain.class , args) ;
    }
}
