package com.lxh.cloud.service.servicevod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.vod.model.v20170321.*;
import com.google.gson.Gson;
import com.lxh.cloud.service.servicevod.service.VodService;
import com.lxh.cloud.service.servicevod.utils.ConstantVodUtils;
import com.lxh.cloud.service.servicevod.utils.VideoClient;
import com.lxh.common.cmomonutils.CommonResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/vodservice/video")
public class VodController {

    @Autowired
    private VodService vodService;

    //上传视频到阿里云
    @PostMapping("/uploadAlyiVideo")
    public CommonResult uploadAlyiVideo(MultipartFile file) {
        //返回上传视频id
        String videoId = vodService.uploadVideoAly(file);
        return CommonResult.ok().data("videoId", videoId);
    }

    //上传视频到阿里云
    @DeleteMapping("/deleteAlyiVideo/{id}")
    public CommonResult deleteAlyiVideo(@PathVariable("id") String id) {
        //返回上传视频id
        boolean flag = vodService.deleteVideo(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }


    @GetMapping("getPlayAuth/{videoId}")
    public CommonResult getVideoPlayAuth(@PathVariable("videoId") String videoId) throws Exception {
        //获取阿里云存储相关常量
        String accessKeyId = ConstantVodUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantVodUtils.ACCESS_KEY_SECRET;
        //初始化
        DefaultAcsClient client = VideoClient.initVodClient(accessKeyId, accessKeySecret);
        //请求
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId(videoId);
        //响应
        GetVideoPlayAuthResponse response = client.getAcsResponse(request);
        //得到播放凭证
        String playAuth = response.getPlayAuth();
        System.out.print("PlayAuth = " + response.getPlayAuth() + "\n");
        //VideoMeta信息
        System.out.print("VideoMeta.Title = " + response.getVideoMeta().getTitle() + "\n");
        //返回结果
        return CommonResult.ok().message("获取凭证成功").data("playAuth", playAuth);
    }


    @GetMapping("getVideoPlayAuthUrl/{videoId}")
    public CommonResult getVideoPlayAuthUrl(@PathVariable("videoId") String videoId) throws Exception {
        //获取阿里云存储相关常量
        DefaultProfile profile = DefaultProfile.getProfile(
                "cn-Shanghai",                // // 点播服务所在的地域ID，中国大陆地域请填cn-shanghai
                ConstantVodUtils.ACCESS_KEY_ID,        // 您的AccessKey ID
                ConstantVodUtils.ACCESS_KEY_SECRET );    // 您的AccessKey Secret
        IAcsClient client = new DefaultAcsClient(profile);
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        String playUrl = null;
        // 视频ID。
        request.setVideoId(videoId);
        try {
            GetPlayInfoResponse response = client.getAcsResponse(request);
            System.out.println(new Gson().toJson(response));
            for (GetPlayInfoResponse.PlayInfo playInfo : response.getPlayInfoList()) {
                // 播放地址
                playUrl= playInfo.getPlayURL();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //返回结果
        return CommonResult.ok().message("获取凭证成功").data("playAuth", playUrl);
    }


/*
    @PostMapping("/download")
    public String downLoad(String fileName, HttpServletResponse response) throws UnsupportedEncodingException {
        String filename= fileName;
        File file = new File(filename);
        if(file.exists()){ //判断文件父目录是否存在
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            // response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment;fileName=" +   java.net.URLEncoder.encode(filename,"UTF-8"));
            byte[] buffer = new byte[1024];
            FileInputStream fis = null; //文件输入流
            BufferedInputStream bis = null;

            OutputStream os = null; //输出流
            try {
                os = response.getOutputStream();
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while(i != -1){
                    os.write(buffer);
                    i = bis.read(buffer);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("----------file download---" + filename);
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }
*/

    public static void main(String[] args) throws ClientException {

        String accessKeyId = ConstantVodUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantVodUtils.ACCESS_KEY_SECRET;
        //初始化
        DefaultAcsClient client = VideoClient.initVodClient("LTAI4FsyzXaSEs3P1oKBNdhB", "kJJvCvR3Go77WHyhypEknI6F5H3J0J");
        GetMezzanineInfoRequest request = new GetMezzanineInfoRequest();
        request.setVideoId("afcded7cec9c4f4e931e131db5fe62a4");
        //源片下载地址过期时间
        request.setAuthTimeout(3600L);
        GetMezzanineInfoResponse response = client.getAcsResponse(request);
/*        System.out.print("Mezzanine.VideoId = " + response.getMezzanine().getVideoId() + "\n");
        System.out.print("Mezzanine.FileURL = " + response.getMezzanine().getFileURL() + "\n");
        System.out.print("Mezzanine.Width = " + response.getMezzanine().getWidth() + "\n");
        System.out.print("Mezzanine.Height = " + response.getMezzanine().getHeight() + "\n");
        System.out.print("Mezzanine.Size = " + response.getMezzanine().getSize() + "\n");
        DefaultAcsClient client = initVodClient("<Your AccessKeyId>", "<Your AccessKeySecret>");
        GetMezzanineInfoResponse response = new GetMezzanineInfoResponse();*/
        try {
//            response = getMezzanineInfo(client);
            System.out.print("Mezzanine.VideoId = " + response.getMezzanine().getVideoId() + "\n");
            System.out.print("Mezzanine.FileURL = " + response.getMezzanine().getFileURL() + "\n");
            System.out.print("Mezzanine.Width = " + response.getMezzanine().getWidth() + "\n");
            System.out.print("Mezzanine.Height = " + response.getMezzanine().getHeight() + "\n");
            System.out.print("Mezzanine.Size = " + response.getMezzanine().getSize() + "\n");
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }


 /*   @PostMapping("/download")
    public String  httpDownload(String httpUrl,HttpServletResponse response) throws IOException {
        // 1.下载网络文件
        int byteRead;
        URL url;
        try {
            url = new URL(httpUrl);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
            return null;
        }
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            // response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment;fileName=" +   java.net.URLEncoder.encode("aa.mp4","UTF-8"));

            //2.获取链接
            URLConnection conn = url.openConnection();
            //3.输入流
            InputStream fis = conn.getInputStream();
            //3.写入文件
//            FileOutputStream fs = new FileOutputStream(saveFile);


            byte[] buffer = new byte[1024];
            BufferedInputStream bis = null;

            OutputStream os = null; //输出流
            try {
                os = response.getOutputStream();
                bis = new BufferedInputStream(fis);
                int i = bis.read(buffer);
                while(i != -1){
                    os.write(buffer);
                    i = bis.read(buffer);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("----------file download---");
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        return null;
    }*/

}
