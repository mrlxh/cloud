package com.lxh.cloud.service.servicelike.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.cloud.service.servicelike.entity.UserArticle;

/**
 * <p>
 * 收藏 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface UserArticleMapper extends BaseMapper<UserArticle> {

}
