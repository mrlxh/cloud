package com.lxh.cloud.service.servicelike.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicelike.entity.UserArticle;
import com.lxh.cloud.service.servicelike.qo.ArticleQueryObject;
import com.lxh.cloud.service.servicelike.service.UserArticleService;
import com.lxh.common.cmomonutils.CommonResult;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 收藏 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@RestController
@RequestMapping("/like/servicearticle/user-article")
public class UserArticleController {

    @Autowired
    private UserArticleService userArticleService;


    @ApiOperation(value = "修改收藏信息")
    @PutMapping("/updateArticle/{id}")
    public CommonResult updateArticle(@PathVariable String id, @RequestBody UserArticle userArticle){
        userArticle.setId(id);
        boolean b = userArticleService.updateById(userArticle);
        return b ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "删除收藏信息")
    @PutMapping("/deleteArticle/{id}")
    public CommonResult deleteArticle(@PathVariable String id){
        boolean b = userArticleService.removeById(id);
        return b ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "分页查询用户信息")
    @PostMapping("/pageArticle")
    public CommonResult pageArticle(
            @ApiParam(name = "ArticleQueryObject", value = "查询用户对象", required = true)
            @RequestBody ArticleQueryObject articleQueryObject) {
        Page<UserArticle> page=new Page<>(articleQueryObject.getCurrent(),articleQueryObject.getLimit());
        QueryWrapper<UserArticle> wrapper = new QueryWrapper<>();
        wrapper.like("article_id",articleQueryObject.getArticleId());
        userArticleService.page(page,wrapper);
        long total = page.getTotal();
        List<UserArticle> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

}
