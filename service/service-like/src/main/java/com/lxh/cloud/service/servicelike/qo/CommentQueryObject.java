package com.lxh.cloud.service.servicelike.qo;
import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author Lee
 * @date 2020/4/7 -17:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "评论查询对象")
public class CommentQueryObject extends QueryObject{

    @ApiModelProperty(value = "用户ID")
    private String userId;

}
