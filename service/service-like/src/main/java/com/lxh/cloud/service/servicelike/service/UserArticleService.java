package com.lxh.cloud.service.servicelike.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.servicelike.entity.UserArticle;

/**
 * <p>
 * 收藏 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface UserArticleService extends IService<UserArticle> {

}
