package com.lxh.cloud.service.servicelike.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.servicelike.entity.ArticleComment;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
public interface ArticleCommentService extends IService<ArticleComment> {

}
