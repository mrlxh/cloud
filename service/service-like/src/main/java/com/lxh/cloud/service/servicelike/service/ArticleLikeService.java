package com.lxh.cloud.service.servicelike.service;

import com.lxh.cloud.service.servicelike.entity.ArticleLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点赞 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
public interface ArticleLikeService extends IService<ArticleLike> {

}
