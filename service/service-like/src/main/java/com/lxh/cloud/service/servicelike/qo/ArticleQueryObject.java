package com.lxh.cloud.service.servicelike.qo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "文章数据分页")
public class ArticleQueryObject extends QueryObject {

    @ApiModelProperty(value = "文章id")
    private String articleId;

}
