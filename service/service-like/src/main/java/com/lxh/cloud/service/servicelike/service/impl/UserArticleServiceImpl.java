package com.lxh.cloud.service.servicelike.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.cloud.service.servicelike.entity.UserArticle;
import com.lxh.cloud.service.servicelike.mapper.UserArticleMapper;
import com.lxh.cloud.service.servicelike.service.UserArticleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@Service
public class UserArticleServiceImpl extends ServiceImpl<UserArticleMapper, UserArticle> implements UserArticleService {

}
