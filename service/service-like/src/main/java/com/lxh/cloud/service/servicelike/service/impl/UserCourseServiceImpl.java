package com.lxh.cloud.service.servicelike.service.impl;

import com.lxh.cloud.service.servicelike.entity.UserCourse;
import com.lxh.cloud.service.servicelike.mapper.UserCourseMapper;
import com.lxh.cloud.service.servicelike.service.UserCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@Service
public class UserCourseServiceImpl extends ServiceImpl<UserCourseMapper, UserCourse> implements UserCourseService {

}
