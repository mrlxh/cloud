package com.lxh.cloud.service.servicelike.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicelike.entity.UserCourse;
import com.lxh.cloud.service.servicelike.qo.CourseQueryObject;
import com.lxh.cloud.service.servicelike.service.UserCourseService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
 * <p>
 * 收藏 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
@RestController
@RequestMapping("/like/servicecourse/user-course")
public class UserCourseController {
    @Autowired
    private UserCourseService userCourseService;

    @ApiOperation(value = "分页查询收藏信息")
    @PostMapping("/pageCourse")
    public CommonResult pageCourse(
            @ApiParam(name = "CourseQueryObject", value = "查询收藏对象", required = true)
                @RequestBody CourseQueryObject courseQueryObject) {
        Page<UserCourse> page = new Page<>(courseQueryObject.getCurrent(),courseQueryObject.getLimit());
        QueryWrapper<UserCourse> wrapper = new QueryWrapper<>();
        wrapper.like("user_id", courseQueryObject.getUserId());
        userCourseService.page(page,wrapper);
        long total = page.getTotal();
        List<UserCourse> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation(value = "添加收藏信息")
    @PostMapping("/courseInsert")
    public CommonResult courseInsert(
            @ApiParam(name = "usercourse", value = "添加收藏信息", required = true)
            @RequestBody UserCourse usercourse) {
        boolean flag = userCourseService.save(usercourse);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "删除反馈信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "删除反馈信息", required = true)
            @PathVariable String id) {
        boolean flag = userCourseService.removeById(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

}

