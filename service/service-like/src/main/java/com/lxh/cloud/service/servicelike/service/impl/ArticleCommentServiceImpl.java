package com.lxh.cloud.service.servicelike.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.cloud.service.servicelike.entity.ArticleComment;
import com.lxh.cloud.service.servicelike.mapper.ArticleCommentMapper;
import com.lxh.cloud.service.servicelike.service.ArticleCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
@Service
public class ArticleCommentServiceImpl extends ServiceImpl<ArticleCommentMapper, ArticleComment> implements ArticleCommentService {

}
