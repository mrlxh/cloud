package com.lxh.cloud.service.servicelike.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxh.cloud.service.servicelike.entity.ArticleComment;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
public interface ArticleCommentMapper extends BaseMapper<ArticleComment> {

}
