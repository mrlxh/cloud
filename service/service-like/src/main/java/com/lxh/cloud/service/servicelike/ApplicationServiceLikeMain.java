package com.lxh.cloud.service.servicelike;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Lee
 * @date 2020/4/5 -16:28
 */
@MapperScan("com.lxh.cloud.service.servicelike.mapper")
@ComponentScan("com.lxh")
@SpringBootApplication
@EnableDiscoveryClient
public class ApplicationServiceLikeMain {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationServiceLikeMain.class , args) ;
    }
}
