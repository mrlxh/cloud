package com.lxh.cloud.service.servicelike.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicelike.entity.ArticleLike;
import com.lxh.cloud.service.servicelike.qo.LikeQueryObject;
import com.lxh.cloud.service.servicelike.service.ArticleLikeService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 点赞 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/like/servicelike/article-like")
public class ArticleLikeController {

    @Autowired
    private ArticleLikeService articleLikeService;

    @ApiOperation(value = "分页查询点赞信息")
        @PostMapping("/pageLike")
    public CommonResult pageLike(
            @ApiParam(name = "likeQueryObject", value = "查询点赞对象", required = true)
            @RequestBody LikeQueryObject likeQueryObject) {
        Page<ArticleLike> page = new Page<>(likeQueryObject.getCurrent(),likeQueryObject.getLimit());
        QueryWrapper<ArticleLike> wrapper = new QueryWrapper<>();
        wrapper.like("user_id", likeQueryObject.getUserId());
        articleLikeService.page(page,wrapper);
        long total = page.getTotal();
        List<ArticleLike> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation(value = "删除反馈信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "删除反馈信息", required = true)
            @PathVariable String id) {
        boolean flag = articleLikeService.removeById(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }
}

