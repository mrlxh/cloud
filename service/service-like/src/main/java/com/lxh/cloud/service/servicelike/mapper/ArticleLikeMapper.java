package com.lxh.cloud.service.servicelike.mapper;

import com.lxh.cloud.service.servicelike.entity.ArticleLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点赞 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
public interface ArticleLikeMapper extends BaseMapper<ArticleLike> {

}
