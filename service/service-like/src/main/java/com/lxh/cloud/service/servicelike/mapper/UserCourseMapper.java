package com.lxh.cloud.service.servicelike.mapper;

import com.lxh.cloud.service.servicelike.entity.UserCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收藏 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface UserCourseMapper extends BaseMapper<UserCourse> {

}
