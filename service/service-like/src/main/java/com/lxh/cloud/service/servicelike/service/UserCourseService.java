package com.lxh.cloud.service.servicelike.service;

import com.lxh.cloud.service.servicelike.entity.UserCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-05-01
 */
public interface UserCourseService extends IService<UserCourse> {

}
