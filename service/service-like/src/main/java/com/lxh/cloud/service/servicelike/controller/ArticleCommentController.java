package com.lxh.cloud.service.servicecomment.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.lxh.cloud.service.servicelike.entity.ArticleComment;
import com.lxh.cloud.service.servicelike.qo.CommentQueryObject;
import com.lxh.cloud.service.servicelike.service.ArticleCommentService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/like/servicecomment/article-comment")
public class ArticleCommentController {

    @Autowired
    private ArticleCommentService articleCommentService;

    @ApiOperation(value = "分页查询收藏信息")
    @PostMapping("/pageComment")
    public CommonResult pageComment(
            @ApiParam(name = "CommentQueryObject", value = "查询收藏对象", required = true)
            @RequestBody CommentQueryObject commentQueryObject) {
        Page<ArticleComment> page = new Page<>(commentQueryObject.getCurrent(),commentQueryObject.getLimit());
        QueryWrapper<ArticleComment> wrapper = new QueryWrapper<>();
        wrapper.like("user_id", commentQueryObject.getUserId());
        articleCommentService.page(page,wrapper);
        long total = page.getTotal();
        List<ArticleComment> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation(value = "删除反馈信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "删除反馈信息", required = true)
            @PathVariable String id) {
        boolean flag = articleCommentService.removeById(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "修改反馈信息")
    @PutMapping("/update/{id}")
    public CommonResult update(
            @ApiParam(name = "id", value = "反馈id", required = true)
            @PathVariable String id,
            @ApiParam(name = "comment", value = "修改反馈信息", required = true)
            @RequestBody ArticleComment comment) {
        comment.setId(id);
        boolean flag = articleCommentService.updateById(comment);
        return flag ? CommonResult.ok() : CommonResult.error();
    }
}

