package com.lxh.cloud.service.servicelike.service.impl;

import com.lxh.cloud.service.servicelike.entity.ArticleLike;
import com.lxh.cloud.service.servicelike.mapper.ArticleLikeMapper;
import com.lxh.cloud.service.servicelike.service.ArticleLikeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 点赞 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-05-02
 */
@Service
public class ArticleLikeServiceImpl extends ServiceImpl<ArticleLikeMapper, ArticleLike> implements ArticleLikeService {

}
