package com.lxh.cloud.service.servicecms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicecms.entity.CrmBanner;
import com.lxh.cloud.service.servicecms.qo.Ques;
import com.lxh.cloud.service.servicecms.service.CrmBannerService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author Lee
 * @since 2020-04-13
 */
@RestController
@RequestMapping("/servicecms/crmBanner")
public class CrmBannerController {

    @Autowired
    private CrmBannerService crmBannerService;

    /**
     * 列表查询
     * @return
     */
    @ApiOperation(value = "获取banner")
    @Cacheable(value = "banner", key = "'selectIndexList'")
    @PostMapping("/getSelectList")
    public CommonResult getBannerList(){
        QueryWrapper<CrmBanner> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("sort").last("limit 0,3");
        return CommonResult.ok().data("bannerList",crmBannerService.list(wrapper));

    }

    /**
     * 分页查询
     * @param ques
     * @return
     */
    @ApiOperation(value = "分页查询banner")
    @PostMapping("/getBannerList")
    public CommonResult getBannerList(
            @ApiParam(name = "ques", value = "查询banner图片", required = true)
            @RequestBody Ques ques){

        Page<CrmBanner> page = new Page<>(ques.getCurrent(), ques.getLimit());

        crmBannerService.pageQuery(page,ques);

        long total = page.getTotal();
        List<CrmBanner> records = page.getRecords();

        return CommonResult.ok().data("total", total).data("rows", records);

    }


    /**
     * 根据id删除图片
     * @param id
     * @return
     */
    @ApiOperation(value = "删除一个banner")
    @DeleteMapping("/getBannerId/{id}")
    public CommonResult getBannerId(@ApiParam(name="id", value="bannerId",required = true)
                                    @PathVariable String id){
        boolean flag = crmBannerService.removeById(id);
        return flag ? CommonResult.ok() : CommonResult.error();
    }


    /**
     * 添加banner
     * @param crmBanner
     * @return
     */
    @ApiOperation(value = "添加一个banner")
    @PostMapping("/addBanner")
    public CommonResult addBanner(
            @ApiParam(name="crmBanner", value="banner数据",required = true)
            @RequestBody CrmBanner crmBanner){
        boolean flag = crmBannerService.save(crmBanner);
        return flag ? CommonResult.ok() : CommonResult.error();
    }


    /**
     *根据id修改
     * @param id
     * @param crmBanner
     * @return
     */
    @ApiOperation(value = "修改banner信息")
    @PutMapping("/updateBanner/{id}")
    public CommonResult updateBanner(
            @ApiParam(name = "id", value = "bannerId", required = true)
            @PathVariable String id,
            @ApiParam(name="crmBanner", value="banner数据",required = true)
            @RequestBody CrmBanner crmBanner){
        crmBanner.setId(id);
        boolean flag = crmBannerService.updateById(crmBanner);
        return flag ? CommonResult.ok() : CommonResult.error();
    }
}

