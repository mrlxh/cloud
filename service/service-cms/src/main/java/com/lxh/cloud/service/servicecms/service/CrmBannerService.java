package com.lxh.cloud.service.servicecms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicecms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.servicecms.qo.Ques;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author Lee
 * @since 2020-04-13
 */
public interface CrmBannerService extends IService<CrmBanner> {


    void pageQuery(Page<CrmBanner> page, Ques ques);
}
