package com.lxh.cloud.service.servicecms.qo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "banner对象")
public class Ques extends QueryObject {

    @ApiModelProperty(value = "标题")
    private String title;


}
