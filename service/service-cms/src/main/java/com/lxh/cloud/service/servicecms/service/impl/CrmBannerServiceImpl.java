package com.lxh.cloud.service.servicecms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.servicecms.entity.CrmBanner;
import com.lxh.cloud.service.servicecms.mapper.CrmBannerMapper;
import com.lxh.cloud.service.servicecms.qo.Ques;
import com.lxh.cloud.service.servicecms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author Lee
 * @since 2020-04-13
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Override
    public void pageQuery(Page<CrmBanner> page, Ques ques) {

        QueryWrapper<CrmBanner> wrapper = new QueryWrapper<>();

        //根据排序倒叙
        wrapper.orderByDesc("sort");

        if (!StringUtils.isEmpty(ques.getTitle())) {
            wrapper.like("title", ques.getTitle());
        }
        baseMapper.selectPage(page, wrapper);
    }

}
