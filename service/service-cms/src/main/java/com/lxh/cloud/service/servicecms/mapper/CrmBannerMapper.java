package com.lxh.cloud.service.servicecms.mapper;

import com.lxh.cloud.service.servicecms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author Lee
 * @since 2020-04-13
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
