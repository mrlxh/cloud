package com.lxh.cloud.service.serviceuser.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.serviceuser.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginInfoVo;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginVo;
import com.lxh.cloud.service.serviceuser.entity.vo.RegisterVo;
import com.lxh.cloud.service.serviceuser.qo.UserQueryObject;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-04-05
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    void pageQuery(Page<UcenterMember> pageParam, UserQueryObject userQueryObject);

    String login(LoginVo vo);

     LoginInfoVo getLoginInfo(String memberId);

    boolean register(RegisterVo registerVo);

    Integer countRegisterDay(String day);
}
