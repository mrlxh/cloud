package com.lxh.cloud.service.serviceuser.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.lxh.cloud.common.servicebase.exceptionhandler.MyException;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginInfoVo;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginVo;
import com.lxh.cloud.service.serviceuser.entity.vo.RegisterVo;
import com.lxh.cloud.service.serviceuser.service.UcenterMemberService;
import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.common.cmomonutils.utils.JwtUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Lee
 * @date 2020/4/15 -18:48
 */
@RestController
@RequestMapping("/serviceuser/member")
public class MemberLoginController {

    @Autowired
    private UcenterMemberService ucenterMemberService;

    @PostMapping(value = "/login")
    public CommonResult login(@RequestBody LoginVo loginVo){

        return CommonResult.ok().data("token",ucenterMemberService.login(loginVo));
    }


    @ApiOperation(value = "会员注册")
    @PostMapping("/register")
    public CommonResult register(@RequestBody RegisterVo registerVo){

        boolean flag=ucenterMemberService.register(registerVo);

        return flag?CommonResult.ok():CommonResult.error();

    }

    @ApiOperation(value = "根据token获取登录信息")
    @GetMapping("/getLoginInfo")
    public CommonResult getLoginInfo(HttpServletRequest request){
        try {
            String memberId = JwtUtils.getMemberIdByJwtToken(request);
            LoginInfoVo loginVo = ucenterMemberService.getLoginInfo(memberId);
            return CommonResult.ok().data("item", loginVo);
        }catch (Exception e){
            e.printStackTrace();
            throw new MyException(20001,"error");
        }

    }
}
