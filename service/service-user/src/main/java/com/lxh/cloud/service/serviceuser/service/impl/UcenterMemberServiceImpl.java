package com.lxh.cloud.service.serviceuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.common.servicebase.exceptionhandler.MyException;
import com.lxh.cloud.service.serviceuser.entity.UcenterMember;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginInfoVo;
import com.lxh.cloud.service.serviceuser.entity.vo.LoginVo;
import com.lxh.cloud.service.serviceuser.entity.vo.RegisterVo;
import com.lxh.cloud.service.serviceuser.mapper.UcenterMemberMapper;
import com.lxh.cloud.service.serviceuser.qo.UserQueryObject;
import com.lxh.cloud.service.serviceuser.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxh.common.cmomonutils.utils.JwtUtils;
import com.lxh.common.cmomonutils.utils.MD5;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-04-05
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void pageQuery(Page<UcenterMember> pageParam, UserQueryObject userQueryObject) {

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();

        //根据添加时间倒叙
        wrapper.orderByDesc("gmt_create");
        if (!StringUtils.isEmpty(userQueryObject.getName())) {
            wrapper.like("nickname", userQueryObject.getName());
        }
        if (!StringUtils.isEmpty(userQueryObject.getPhone())) {
            wrapper.eq("mobile", userQueryObject.getPhone());
        }
        if (!StringUtils.isEmpty(userQueryObject.getBegin())) {
            wrapper.ge("gmt_create", userQueryObject.getBegin());
        }
        if (!StringUtils.isEmpty(userQueryObject.getEnd())) {
            wrapper.le("gmt_create", userQueryObject.getEnd());
        }
        baseMapper.selectPage(pageParam, wrapper);
    }

    @Override
    public String login(LoginVo vo) {

        String phone = vo.getPhone();

        String password = vo.getPassword();

        if (StringUtils.isEmpty(phone)) {
            throw new MyException(20001, "手机号不能为空");
        }
        if (StringUtils.isEmpty(password)) {
            throw new MyException(20001, "密码不能为空");
        }

        UcenterMember member = baseMapper.selectOne(new QueryWrapper<UcenterMember>().eq("mobile", phone));

        if (null == member) {
            throw new MyException(20001, "没有该用户");
        }
        //校验是否被禁用
        if (member.getIsDisabled() == 1) {
            throw new MyException(20001, "你的账号被禁用了，请联系管理员");
        }
        //验证密码
        if (!member.getPassword().equals(password)) {
            throw new MyException(20001, "密码错误");

        }
        //使用JWT生成token字符串
        boolean flag=baseMapper.addLoginNum();

        return JwtUtils.getJwtToken(member.getId(), member.getNickname());
    }

    @Override
    public LoginInfoVo getLoginInfo(String memberId) {

        UcenterMember ucenterMember = baseMapper.selectById(memberId);

        LoginInfoVo loginInfoVo = new LoginInfoVo();
        BeanUtils.copyProperties(ucenterMember, loginInfoVo);

        return loginInfoVo;

    }

    @Override
    public boolean register(RegisterVo registerVo) {

        //获取注册信息，进行校验
        String nickname = registerVo.getNickname();

        String mobile = registerVo.getMobile();

        String password = registerVo.getPassword();

        String code = registerVo.getCode();

        //校验参数
        if (StringUtils.isEmpty(mobile) ||

                StringUtils.isEmpty(password) ||

                StringUtils.isEmpty(code)) {

            throw new MyException(20001, "error");

        }
        //校验校验验证码
        //从redis获取发送的验证码
        String mobleCode = redisTemplate.opsForValue().get(mobile);

        if (!code.equals(mobleCode)) {
            throw new MyException(20001, "验证码错误");
        }
        //查询数据库中是否存在相同的手机号码
        Integer count = baseMapper.selectCount(new QueryWrapper<UcenterMember>().eq("mobile", mobile));
        if (count > 0) {
            throw new MyException(20001, "该号码已存在");
        }
        //添加注册信息到数据库
        UcenterMember member = new UcenterMember();

        member.setNickname(nickname);

        member.setMobile(registerVo.getMobile());

        member.setPassword(password);
//        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(0);

        member.setAvatar("https://lxh--cloud.oss-cn-beijing.aliyuncs.com/2020/04/09/d9608b0090a24217bd9730f41dffd690file.png");

        return this.save(member);
    }

    //查询某一天注册人数
    @Override
    public Integer countRegisterDay(String day) {
        return baseMapper.countRegisterDay(day);
    }

}


