package com.lxh.cloud.service.serviceuser.qo;

import com.lxh.cloud.common.servicebase.entity.QueryObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lee
 * @date 2020/4/7 -17:49
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用户查询对象")
public class UserQueryObject extends QueryObject{

    @ApiModelProperty(value = "用户姓名")
    private String name;

    @ApiModelProperty(value = "用户电话")
    private String phone;

    @ApiModelProperty(value = "注册时间-开始")
    private Date begin;

    @ApiModelProperty(value = "注册时间-结束")
    private Date end;
}
