package com.lxh.cloud.service.serviceuser.entity.vo;

import lombok.Data;

/**
 * @author Lee
 * @date 2020/4/15 -18:50
 */
@Data
public class LoginVo {

    private String phone;

    private String password;
}
