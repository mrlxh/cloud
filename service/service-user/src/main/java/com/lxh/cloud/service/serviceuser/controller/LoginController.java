package com.lxh.cloud.service.serviceuser.controller;

import com.lxh.common.cmomonutils.CommonResult;
import org.springframework.web.bind.annotation.*;



/**
 * @author Lee
 * @date 2020/4/7 -16:25
 */
@RestController
@RequestMapping("/serviceuser")
public class LoginController {

    @PostMapping(value = "/user/login")
    public CommonResult login(){

        return CommonResult.ok().data("token","admin");
    }

    @GetMapping(value = "/user/info")
    public CommonResult info(){
        return CommonResult.ok().data("roles","[admin]").data("name","admin").data("avatar","");
    }
}
