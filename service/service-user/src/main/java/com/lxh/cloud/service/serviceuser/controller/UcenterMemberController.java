package com.lxh.cloud.service.serviceuser.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxh.cloud.service.serviceuser.entity.UcenterMember;
import com.lxh.cloud.service.serviceuser.qo.UserQueryObject;
import com.lxh.cloud.service.serviceuser.service.UcenterMemberService;
import com.lxh.common.cmomonutils.CommonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-04-05
 */
@RestController
@RequestMapping("/serviceuser")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService ucenterMemberService;

    @ApiOperation(value = "所有用户信息")
    @GetMapping("/findAll")
    public CommonResult findAll() {
        List<UcenterMember> list = ucenterMemberService.list(null);
        return CommonResult.ok().data("list",list);
    }

    @ApiOperation(value = "分页查询用户信息")
    @PostMapping("/pageUser")
    public CommonResult pageUser(
            @ApiParam(name = "userQueryObject", value = "查询用户对象", required = true)
              @RequestBody UserQueryObject userQueryObject) {
        Page<UcenterMember> page = new Page<>(userQueryObject.getCurrent(), userQueryObject.getLimit());
        ucenterMemberService.pageQuery(page,userQueryObject);
        long total = page.getTotal();
        List<UcenterMember> records = page.getRecords();
        return CommonResult.ok().data("total", total).data("rows", records);
    }

    @ApiOperation(value = "删除用户信息")
    @DeleteMapping("/delete/{id}")
    public CommonResult delete(
            @ApiParam(name = "id", value = "用户id", required = true)
            @PathVariable String id) {

        boolean flag = ucenterMemberService.removeById(id);

        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "修改用户信息")
    @PutMapping("/update/{id}")
    public CommonResult update(
            @ApiParam(name = "id", value = "用户id", required = true)
            @PathVariable String id,
            @ApiParam(name = "ucenterMember", value = "用户对象", required = true)
                    @RequestBody UcenterMember ucenterMember) {
        ucenterMember.setId(id);
        boolean flag = ucenterMemberService.updateById(ucenterMember);
        return flag ? CommonResult.ok() : CommonResult.error();
    }

    @ApiOperation(value = "添加用户信息")
    @PostMapping("/insert")
    public CommonResult insert(
            @ApiParam(name = "ucenterMember", value = "用户对象", required = true)
                    @RequestBody UcenterMember ucenterMember) {
        boolean flag = ucenterMemberService.save(ucenterMember);
        return flag ? CommonResult.ok() : CommonResult.error();
    }


    //查询某一天注册人数
    @GetMapping("/countRegister/{day}")
    public CommonResult countRegister(@PathVariable String day) {
        Integer count = ucenterMemberService.countRegisterDay(day);
        return CommonResult.ok().data("countRegister",count);
    }
}

