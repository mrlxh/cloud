package com.lxh.cloud.service.serviceoos;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Lee
 * @date 2020/4/5 -16:28
 */
//一直没加出去DruidDataSourceAutoConfigure，，所以不能出去数据库的配置类
@ComponentScan("com.lxh")
@EnableDiscoveryClient
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class })
public class ApplicationServiceOosMain {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationServiceOosMain.class , args) ;
    }
}
