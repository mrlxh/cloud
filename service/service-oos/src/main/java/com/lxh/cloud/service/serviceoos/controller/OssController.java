package com.lxh.cloud.service.serviceoos.controller;

import com.lxh.cloud.service.serviceoos.service.OssService;
import com.lxh.common.cmomonutils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Lee
 * @date 2020/4/8 -17:27
 */
@RestController
@RequestMapping("/ossservice/file")
public class OssController {

    @Autowired
    private OssService ossService;

    //上传头像的方法
    @PostMapping("/upload")
    public CommonResult uploadOssFile(MultipartFile file) {
        //获取上传文件  MultipartFile
        //返回上传到oss的路径
        String url = ossService.uploadFileAvatar(file);
        return CommonResult.ok().data("url",url);
    }
}
