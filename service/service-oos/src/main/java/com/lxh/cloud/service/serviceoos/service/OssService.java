package com.lxh.cloud.service.serviceoos.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Lee
 * @date 2020/4/8 -17:24
 */
public interface OssService {
    //上传头像到oss
    String uploadFileAvatar(MultipartFile file);
}
