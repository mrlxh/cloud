package com.lxh.cloud.common.servicebase.exceptionhandler;

import com.lxh.common.cmomonutils.CommonResult;
import com.lxh.common.cmomonutils.utils.ExceptionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    //指定出现什么异常执行这个方法
    @ExceptionHandler(Exception.class)
    @ResponseBody //为了返回数据
    public CommonResult error(Exception e) {
        log.error(ExceptionUtils.getMessage(e));
        e.printStackTrace();
        return CommonResult.error().message("服务器繁忙..");
    }


    //自定义异常
    @ExceptionHandler(com.lxh.cloud.common.servicebase.exceptionhandler.MyException.class)
    @ResponseBody //为了返回数据
    public CommonResult error(MyException e) {
        log.error(ExceptionUtils.getMessage(e));
        e.printStackTrace();

        return CommonResult.error().code(e.getCode()).message(e.getMsg());
    }

}
