package com.lxh.cloud.common.servicebase.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Lee
 * @date 2020/4/7 -17:48
 */
@Data
@ApiModel(value="查询对象")
public class QueryObject implements Serializable {

    @ApiModelProperty(value = "当前页")
    private long current;

    @ApiModelProperty(value = "每页条数")
    private long limit;

}
