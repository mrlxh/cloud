package com.lxh.cloud.common.servicebase.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author LXH
 * @date 2019/9/5 -11:24
 */
@Data
public class BaseTreeGrid implements Serializable {

    private static final long serialVersionUID = -9189631784252440402L;

    public String id;//节点id

    public String name;//节点名

    public String parentId;//节点父id

    public String iconCls = "folder";//节点样式，默认即可

    public Boolean leaf = true;//是否为叶子节点，true表示是叶子节点，false表示不是叶子节点

    public Boolean open = false; //是否展开，

    public Integer sort;

    public List<BaseTreeGrid> children;//孩子节点

}

