package com.lxh.common.cmomonutils.constant;

/**
 * @author Lee
 * @date 2020/4/5 -17:24
 */
public class ResultCode {

    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
