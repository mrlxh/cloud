package com.lxh.cloud.base.apigateway;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Lee
 * @date 2020/4/17 -20:22
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ApplicationGateway {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationGateway.class , args) ;
    }
}
